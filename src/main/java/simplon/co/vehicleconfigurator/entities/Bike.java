package simplon.co.vehicleconfigurator.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrimaryKeyJoinColumn;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@PrimaryKeyJoinColumn(name = "vehicle_id")
@OnDelete(action = OnDeleteAction.CASCADE)
public class Bike extends Vehicle {

    public enum bikeType {
        ROADBIKE, CITYBIKE, MOUNTAINBIKE
    }

    @Column(columnDefinition = "ENUM('ROADBIKE', 'CITYBIKE', 'MOUNTAINBIKE')")
    @Enumerated(EnumType.STRING)
    private bikeType type;

    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private int plates;

    @Column(nullable = false)
    private boolean antiTheft;

    public Bike(bikeType type, int plates, boolean antiTheft) {
        this.type = type;
        this.plates = plates;
        this.antiTheft = antiTheft;
    }

    public Bike() {
    }

    public bikeType getType() {
        return type;
    }

    public void setType(bikeType type) {
        this.type = type;
    }

    public int getPlates() {
        return plates;
    }

    public void setPlates(int plates) {
        this.plates = plates;
    }

    public boolean isAntiTheft() {
        return antiTheft;
    }

    public void setAntiTheft(boolean antiTheft) {
        this.antiTheft = antiTheft;
    }
}
