package simplon.co.vehicleconfigurator.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.validation.constraints.Size;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@PrimaryKeyJoinColumn(name = "vehicle_id")
@OnDelete(action = OnDeleteAction.CASCADE)
public class Car extends Vehicle {
    
    public enum carType {
        SEDAN, COUPE, CONVERTIBLE, ELECTRIC
    }

    @Column(columnDefinition = "ENUM('SEDAN', 'COUPE', 'CONVERTIBLE', 'ELECTRIC')")
    @Enumerated(EnumType.STRING)
    private carType type;

    @Column(nullable = false)
    @Size(min = 3, max = 7)
    private int door;

    @Column(nullable = false)
    private boolean gearbox;

    @Column(nullable = false, columnDefinition="SMALLINT(4)")
    private int horsepower;

    public Car(carType type, @Size(min = 3, max = 7) int door, boolean gearbox, int horsepower) {
        this.type = type;
        this.door = door;
        this.gearbox = gearbox;
        this.horsepower = horsepower;
    }

    public Car() {
    }

    public carType getType() {
        return type;
    }

    public void setType(carType type) {
        this.type = type;
    }

    public int getDoor() {
        return door;
    }

    public void setDoor(int door) {
        this.door = door;
    }

    public boolean isGearbox() {
        return gearbox;
    }

    public void setGearbox(boolean gearbox) {
        this.gearbox = gearbox;
    }

    public int getHorsepower() {
        return horsepower;
    }

    public void setHorsepower(int horsepower) {
        this.horsepower = horsepower;
    }

}
