package simplon.co.vehicleconfigurator.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrimaryKeyJoinColumn;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@PrimaryKeyJoinColumn(name = "vehicle_id")
@OnDelete(action = OnDeleteAction.CASCADE)
public class Motorcycle extends Vehicle {
    
    public enum carType {
        STANDARD, CRUISER, SPORT, TOURING, SCOOTER
    }

    @Column(columnDefinition = "ENUM('STANDARD', 'CRUISER', 'SPORT', 'TOURING', 'SCOOTER')")
    @Enumerated(EnumType.STRING)
    private carType type;

    @Column(nullable = false, columnDefinition = "SMALLINT")
    private int cylinder;

    @Column(nullable = false, columnDefinition = "SMALLINT")
    private int horsepower;

    public Motorcycle(carType type, int cylinder, int horsepower) {
        this.type = type;
        this.cylinder = cylinder;
        this.horsepower = horsepower;
    }

    public Motorcycle() {
    }

    public carType getType() {
        return type;
    }

    public void setType(carType type) {
        this.type = type;
    }

    public int getCylinder() {
        return cylinder;
    }

    public void setCylinder(int cylinder) {
        this.cylinder = cylinder;
    }

    public int getHorsepower() {
        return horsepower;
    }

    public void setHorsepower(int horsepower) {
        this.horsepower = horsepower;
    }


}
