package simplon.co.vehicleconfigurator.entities;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrimaryKeyJoinColumn;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@PrimaryKeyJoinColumn(name = "vehicle_id")
@OnDelete(action = OnDeleteAction.CASCADE)
@Inheritance(strategy = InheritanceType.JOINED)
public class Motorized extends Vehicle {

    @Column
    private int mileage;

    @Column
    private Date releaseDate;

    public Motorized(int mileage, Date releaseDate) {
        this.mileage = mileage;
        this.releaseDate = releaseDate;
    }

    public Motorized() {
    }

    public int getMileage() {
        return mileage;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }
}
