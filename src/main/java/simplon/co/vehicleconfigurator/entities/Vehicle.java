package simplon.co.vehicleconfigurator.entities;

import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Vehicle {
    @Id
    private int id;

    public enum vehicleType {
        BIKE, CAR, MOTORCYCLE
    }

    @Column(columnDefinition = "ENUM('MOTORBIKE','BICYCLE','CAR')")
    @Enumerated(EnumType.STRING)
    private vehicleType vehicleType;

    @Column(nullable = false)
    private String color;

    @Column(nullable = false)
    private String brand;

    @Column(nullable = false)
    private Float initialPrice;

    @Column()
    private Float resalePrice;

    @Column(nullable = false)
    private LocalTime purchaseDate;

    public Vehicle(String color, String brand, Float initialPrice, Float resalePrice, LocalTime purchaseDate) {
        this.color = color;
        this.brand = brand;
        this.initialPrice = initialPrice;
        this.resalePrice = resalePrice;
        this.purchaseDate = purchaseDate;
    }

    public Vehicle(int id, String color, String brand, Float initialPrice, Float resalePrice, LocalTime purchaseDate) {
        this.id = id;
        this.color = color;
        this.brand = brand;
        this.initialPrice = initialPrice;
        this.resalePrice = resalePrice;
        this.purchaseDate = purchaseDate;
    }

    public Vehicle() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Float getInitialPrice() {
        return initialPrice;
    }

    public void setInitialPrice(Float initialPrice) {
        this.initialPrice = initialPrice;
    }

    public Float getResalePrice() {
        return resalePrice;
    }

    public void setResalePrice(Float resalePrice) {
        this.resalePrice = resalePrice;
    }

    public LocalTime getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(LocalTime purchaseDate) {
        this.purchaseDate = purchaseDate;
    }
}
